(function (window) {
  var declension = {};

  // Функция для подбора формы слова в зависимости от числительного.
  var getCorrectNounForm = function (number, nounForms) {
    var correctForm,
        formDeterminingPartOfNumber = number % 100;

    if (11 <= formDeterminingPartOfNumber && formDeterminingPartOfNumber <= 19) {
      correctForm = nounForms[2];
    } else {
      formDeterminingPartOfNumber %= 10;

      if (formDeterminingPartOfNumber === 1) {
        correctForm = nounForms[0];
      } else if (2 <= formDeterminingPartOfNumber && formDeterminingPartOfNumber <= 4) {
        correctForm = nounForms[1];
      } else {
        correctForm = nounForms[2];
      }
    }

    return correctForm;
  };

  declension.getCorrectNounForm = getCorrectNounForm;

  window.declension = declension;
})(window);