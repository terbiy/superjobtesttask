'use strict';

describe('Directive: favourites', function () {

  // load the directive's module
  beforeEach(module('superJobTestTaskApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<favourites></favourites>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the favourites directive');
  }));
});
