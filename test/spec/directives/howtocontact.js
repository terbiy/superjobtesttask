'use strict';

describe('Directive: howToContact', function () {

  // load the directive's module
  beforeEach(module('superJobTestTaskApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<how-to-contact></how-to-contact>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the howToContact directive');
  }));
});
