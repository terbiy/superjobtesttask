'use strict';

describe('Directive: addedToInbox', function () {

  // load the directive's module
  beforeEach(module('superJobTestTaskApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<added-to-inbox></added-to-inbox>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the addedToInbox directive');
  }));
});
