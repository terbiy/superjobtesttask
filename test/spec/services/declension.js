'use strict';

describe('Service: declension', function () {

  // load the service's module
  beforeEach(module('superJobTestTaskApp'));

  // instantiate service
  var declension;
  beforeEach(inject(function (_declension_) {
    declension = _declension_;
  }));

  it('should do something', function () {
    expect(!!declension).toBe(true);
  });

});
