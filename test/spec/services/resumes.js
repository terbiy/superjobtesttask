'use strict';

describe('Service: resumes', function () {

  // load the service's module
  beforeEach(module('superJobTestTaskApp'));

  // instantiate service
  var resumes;
  beforeEach(inject(function (_resumes_) {
    resumes = _resumes_;
  }));

  it('should do something', function () {
    expect(!!resumes).toBe(true);
  });

});
