'use strict';

describe('Service: workingExperience', function () {

  // load the service's module
  beforeEach(module('superJobTestTaskApp'));

  // instantiate service
  var workingExperience;
  beforeEach(inject(function (_workingExperience_) {
    workingExperience = _workingExperience_;
  }));

  it('should do something', function () {
    expect(!!workingExperience).toBe(true);
  });

});
