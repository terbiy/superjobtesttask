'use strict';

describe('Service: getAccessToResume', function () {

  // load the service's module
  beforeEach(module('superJobTestTaskApp'));

  // instantiate service
  var getAccessToResume;
  beforeEach(inject(function (_getAccessToResume_) {
    getAccessToResume = _getAccessToResume_;
  }));

  it('should do something', function () {
    expect(!!getAccessToResume).toBe(true);
  });

});
