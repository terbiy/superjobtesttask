'use strict';

/**
 * @ngdoc service
 * @name superJobTestTaskApp.resumes
 * @description
 * # resumes
 * Service in the superJobTestTaskApp.
 */
angular.module('superJobTestTaskApp')
  .service('resumes', function ($resource) {
    // Загружаем массив работников из JSON-файла.
    var data = $resource('/data-in-jsons/candidates-info.json');

    var getResume = function (resumes, id) {
      var resume;

      for (let candidate of resumes) {
        if (candidate.id === id) {
          resume = candidate;
        }
      }

      return resume;
    };

    return {
      data: data,
      getResume: getResume
    };
  });
