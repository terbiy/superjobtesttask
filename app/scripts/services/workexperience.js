'use strict';

/**
 * @ngdoc service
 * @name superJobTestTaskApp.workExperience
 * @description
 * # workExperience
 * Service in the superJobTestTaskApp.
 */
angular.module('superJobTestTaskApp')
  .service('workExperience', function ($filter, declension, duration) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var workExperience = {};

    var getYearString = function (years) {
      if (typeof years === "string") {
        years = parseInt(years, 10);
      }
      const YEAR_NOUN_FORMS = ["год", "года", "лет"];
      return declension.getCorrectNounForm(years, YEAR_NOUN_FORMS);
    };

    var getMonthString = function (months) {
      if (typeof months === "string") {
        months = parseInt(months, 10);
      }
      const MONTH_NOUN_FORMS = ["месяц", "месяца", "месяцев"];
      return declension.getCorrectNounForm(months, MONTH_NOUN_FORMS);
    };

    var getYearsAndMonthsString = function (workExperienceDuration) {
      var yearsAndMonthsString = "";

      if (workExperienceDuration.years > 0) {
        let years = workExperienceDuration.years,
            yearsNounForm = getYearString(years);

        yearsAndMonthsString += `${years} ${yearsNounForm}`;
      }

      if (workExperienceDuration.years > 0 && workExperienceDuration.months > 0) {
        yearsAndMonthsString += " ";
      }

      if (workExperienceDuration.months > 0) {
        let months = workExperienceDuration.months,
            monthsNounForm = getMonthString(months);

        yearsAndMonthsString += `${months} ${monthsNounForm}`;
      }

      return yearsAndMonthsString;
    };

    var getFromToString = function (fromToObject) {
      var fromToString = "",
          from = fromToObject.from,
          to = fromToObject.to;
      var fromDate = new Date(from),
          toDate,
          fromString = "",
          toString = "";

      const MONTH_ONLY = "LLLL",
            MONTH_AND_YEAR = "LLLL yyyy";

      var formatDate = function (date, format) {
        return $filter("date")(date, format);
      };

      var setStrings = function (fromFormat, toFormat) {
        fromString = formatDate(fromDate, fromFormat);
        toString = formatDate(toDate, toFormat);
      };

      // На случай, если работник работает в компании по настоящее время.
      // <месяц> <год> — по н.в.
      if (to === "current") {
        toDate = new Date();
        setStrings(MONTH_AND_YEAR, MONTH_AND_YEAR);
        toString = "по н.в.";
      } else {
        toDate = new Date(to);
        // Если работник проработал несколько месяцев в рамках одного года.
        // <месяц> — <месяц> <год>
        if (fromDate.getFullYear() === toDate.getFullYear()) {
          setStrings(MONTH_ONLY, MONTH_AND_YEAR);
        } else {
          // Работник начал работать и закончил в разные годы.
          // <месяц> <год-1> — <месяц> <год-2>
          setStrings(MONTH_AND_YEAR, MONTH_AND_YEAR);
        }
      }

      fromToString = `${fromString} — ${toString}`;
      return fromToString;
    };

    var countDurationAndGetYearsAndMonthsString = function (fromToObject) {
      var workExperienceDuration = duration.yearsAndMonths(fromToObject.from, fromToObject.to),
          yearsAndMonthsString = getYearsAndMonthsString(workExperienceDuration);

      return yearsAndMonthsString;
    };

    var getTotalTimeEmployedString = function (workExperience) {
      var timesEmployed = [];

      for (let work of workExperience) {
        let datesEmployed = work.datesEmployed,
            timeEmployed = duration.yearsAndMonths(datesEmployed.from, datesEmployed.to);

        timesEmployed.push(timeEmployed);
      }

      var totalTimeEmployed = duration.addDurations(...timesEmployed),
          totalTimeEmployedString = getYearsAndMonthsString(totalTimeEmployed);

      return totalTimeEmployedString;
    };

    workExperience.getYearString = getYearString;
    workExperience.getMonthString = getMonthString;
    workExperience.getYearsAndMonthsString = getYearsAndMonthsString;
    workExperience.getFromToString = getFromToString;
    workExperience.countDurationAndGetYearsAndMonthsString = countDurationAndGetYearsAndMonthsString;
    workExperience.getTotalTimeEmployedString = getTotalTimeEmployedString;

    return workExperience;
  });
