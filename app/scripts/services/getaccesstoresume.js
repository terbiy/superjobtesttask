'use strict';

/**
 * @ngdoc service
 * @name superJobTestTaskApp.getAccessToResume
 * @description
 * # getAccessToResume
 * Service in the superJobTestTaskApp.
 */
angular.module('superJobTestTaskApp')
  .service('getAccessToResume', function () {
    var resumesGrantedAccessTo = [];

    var accessToResumeGranted = function (id) {
      return resumesGrantedAccessTo.indexOf(id) > -1;
    };

    this.getAccess = function (id) {
      if (!accessToResumeGranted(id)) {
        resumesGrantedAccessTo.push(id);
      }
    };

    this.hasAccess = function (id) {
      return accessToResumeGranted(id);
    };
  });
