'use strict';

/**
 * @ngdoc service
 * @name superJobTestTaskApp.declension
 * @description
 * # declension
 * Service in the superJobTestTaskApp.
 */
angular.module('superJobTestTaskApp')
  .service('declension', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var declension = {};

    // Функция для подбора формы слова в зависимости от числительного.
    var getCorrectNounForm = function (number, nounForms) {
      var correctForm,
          formDeterminingPartOfNumber = number % 100;

      if (11 <= formDeterminingPartOfNumber && formDeterminingPartOfNumber <= 19) {
        correctForm = nounForms[2];
      } else {
        formDeterminingPartOfNumber %= 10;

        if (formDeterminingPartOfNumber === 1) {
          correctForm = nounForms[0];
        } else if (2 <= formDeterminingPartOfNumber && formDeterminingPartOfNumber <= 4) {
          correctForm = nounForms[1];
        } else {
          correctForm = nounForms[2];
        }
      }

      return correctForm;
    };

    declension.getCorrectNounForm = getCorrectNounForm;

    return declension;
  });
