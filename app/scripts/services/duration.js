'use strict';

/**
 * @ngdoc service
 * @name superJobTestTaskApp.duration
 * @description
 * # duration
 * Service in the superJobTestTaskApp.
 */
angular.module('superJobTestTaskApp')
  .service('duration', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var duration = {};

    const BASIC_YEAR = 1970;

    var stringToDate = function (string) {
      return new Date(string);
    };

    var getFullYear = function (date) {
      return date.getFullYear() - BASIC_YEAR;
    };

    var getTimeBetweenTwoDates = function (fromDate, toDate) {
      var from = new Date(fromDate), to;

      if (toDate === "current") {
        to = new Date();
      } else {
        to = new Date(toDate);
      }

      // В том случае, если не указаны конкретные дни начала работы,
      // получается такая ситуация, что разница между первыми числами
      // соседних месяцов высчитывается не как 1 месяц, а как 0 месяцев.
      if (from.getDate() === 1 && to.getDate() === 1) {
        to.setDate(5);
      }

      return to - from;
    };

    var getDurationInFullYears = function (fromDate, toDate) {
      var timePassed = new Date(getTimeBetweenTwoDates(fromDate, toDate)),
          yearsPassed = getFullYear(timePassed);

      return yearsPassed;
    };

    var getRemainingMonths = function (fromDate, toDate) {
      // Мне необходимо вычислить разницу между датами,
      // убрать из неё полные года, а затем преобразовать
      // в полные месяцы.
      var timePassed = new Date(getTimeBetweenTwoDates(fromDate, toDate)),
          fullYearsDate = new Date(getFullYear(timePassed) + BASIC_YEAR),
          monthsRemaining = new Date(timePassed - fullYearsDate).getMonth();

      return monthsRemaining;
    };

    var getDurationInYearsAndMonths = function (fromDate, toDate) {
      return {
        years: getDurationInFullYears(fromDate, toDate),
        months: getRemainingMonths(fromDate, toDate)
      };
    };

    var addDurations = function (...durations) {
      const MONTHS_PER_YEAR = 12;
      var cumulativeDuration = {
        years: 0,
        months: 0
      };

      for (duration of durations) {
        cumulativeDuration.months += duration.months;
        cumulativeDuration.years += duration.years;
      }

      // Теперь необходимо избыточные месяцы перевести в года,
      // а оставшиеся сохранить.
      var monthsAccumulatedYears = Math.floor(
        cumulativeDuration.months / MONTHS_PER_YEAR
      ),
      remainingMonths = cumulativeDuration.months % MONTHS_PER_YEAR;

      cumulativeDuration.months = remainingMonths;
      cumulativeDuration.years += monthsAccumulatedYears;

      return cumulativeDuration;
    };

    duration.yearsAndMonths = getDurationInYearsAndMonths;
    duration.addDurations = addDurations;
    duration.stringToDate = stringToDate;

    return duration;
  });
