'use strict';

/**
 * @ngdoc service
 * @name superJobTestTaskApp.contacts
 * @description
 * # contacts
 * Service in the superJobTestTaskApp.
 */
angular.module('superJobTestTaskApp')
  .service('contacts', function ($resource) {
    var candidatesInfo = $resource('/data-in-jsons/candidates-contacts.json');

    var getCandidateInfo = function (info, id) {
      var contacts;

      for (let candidate of info) {
        if (candidate.id === id) {
          contacts = candidate;
        }
      }

      return contacts;
    };

    return {
      candidatesInfo: candidatesInfo,
      getCandidateInfo: getCandidateInfo
    };
  });

