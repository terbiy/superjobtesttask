'use strict';

/**
 * @ngdoc service
 * @name superJobTestTaskApp.favourites
 * @description
 * # favourites
 * Service in the superJobTestTaskApp.
 */
angular.module('superJobTestTaskApp')
  .service('favourites', function () {
    var addedToFavourites = [];

    var isInFavourites = function (id) {
      return addedToFavourites.indexOf(id) > -1;
    };

    this.toggleFavourite = function (id) {
      console.log(id);
      if (!isInFavourites(id)) {
        addedToFavourites.push(id);
      } else {
        let idPosition = addedToFavourites.indexOf(id);
        addedToFavourites.splice(idPosition, 1);
      }
    };

    this.inFavourites = function (id) {
      return isInFavourites(id);
    };
  });
