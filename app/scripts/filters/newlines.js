'use strict';

/**
 * @ngdoc filter
 * @name superJobTestTaskApp.filter:newlines
 * @function
 * @description
 * # newlines
 * Filter in the superJobTestTaskApp.
 */
angular.module('superJobTestTaskApp')
  .filter('newlines', function () {
    return function (input) {
      return input.split(/\n/g);
    };
  });
