'use strict';

/**
 * @ngdoc overview
 * @name superJobTestTaskApp
 * @description
 * # superJobTestTaskApp
 *
 * Main module of the application.
 */
angular
  .module('superJobTestTaskApp', [
    'ngLocale',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/resumes', {
        templateUrl: 'views/resumes.html',
        controller: 'ResumesCtrl',
        controllerAs: 'resumes'
      })
      .when('/resumes/:id/view', {
        templateUrl: 'views/resume.html',
        controller: 'ResumeViewCtrl',
        controllerAs: 'resume'
      })
      .when('/404', {
        templateUrl: '404.html',
      })
      .otherwise({
        redirectTo: '/resumes'
      });
  });
