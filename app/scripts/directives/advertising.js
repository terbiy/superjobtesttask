'use strict';

/**
 * @ngdoc directive
 * @name superJobTestTaskApp.directive:advertising
 * @description
 * # advertising
 */
angular.module('superJobTestTaskApp')
  .directive('advertising', function () {
    return {
      templateUrl: './views/advertising.html',
      restrict: 'A'
    };
  });
