'use strict';

/**
 * @ngdoc directive
 * @name superJobTestTaskApp.directive:fullName
 * @description
 * # fullName
 */
angular.module('superJobTestTaskApp')
  .directive('fullName', function () {
    return {
      template: '<span ng-if="hasAccess(candidate.id)">' +
        '{{candidateContacts.familyName}} ' +
        '{{candidateContacts.name}} ' +
        '{{candidateContacts.patronymic}}' +
        '</span>',
      restrict: "A",
      controller: function ($scope, getAccessToResume) {
        $scope.hasAccess = getAccessToResume.hasAccess;
      }
    };
  });
