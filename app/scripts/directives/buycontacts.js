'use strict';

/**
 * @ngdoc directive
 * @name superJobTestTaskApp.directive:buyContacts
 * @description
 * # buyContacts
 */
angular.module('superJobTestTaskApp')
  .directive('buyContacts', function () {
    return {
      templateUrl: "./views/buycontacts.html",
      restrict: "A",
      controller: function ($scope, getAccessToResume) {
        $scope.hasAccess = getAccessToResume.hasAccess;
        $scope.getAccess = getAccessToResume.getAccess;
      }
    };
  });
