'use strict';

/**
 * @ngdoc directive
 * @name superJobTestTaskApp.directive:howToContact
 * @description
 * # howToContact
 */
angular.module('superJobTestTaskApp')
  .directive('howToContact', function () {
    return {
      templateUrl: "./views/howtocontact.html",
      restrict: "A",
      controller: function ($scope, getAccessToResume) {
        $scope.hasAccess = getAccessToResume.hasAccess;
      }
    };
  });
