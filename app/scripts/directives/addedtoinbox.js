'use strict';

/**
 * @ngdoc directive
 * @name superJobTestTaskApp.directive:addedToInbox
 * @description
 * # addedToInbox
 */
angular.module('superJobTestTaskApp')
  .directive('addedToInbox', function () {
    return {
      template: '<span class="added-to-inbox" ng-if="hasAccess(candidate.id)">во «Входящих»</span>',
      restrict: "A",
      controller: function ($scope, getAccessToResume) {
        $scope.hasAccess = getAccessToResume.hasAccess;
      }
    };
  });
