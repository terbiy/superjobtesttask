'use strict';

/**
 * @ngdoc directive
 * @name superJobTestTaskApp.directive:salary
 * @description
 * # salary
 */
angular.module('superJobTestTaskApp')
  .directive('salary', function () {
    return {
      // По пока неясным причинам в Google Chrome 52 на El Capitan пропадает знак рубля после неразрывного пробела, выставленного при помощи кода.
      template: '<span>{{candidate.desiredCompensation | currency: candidate.desiredCompensationCurrency : 0}}</span>',
      restrict: 'A'
    };
  });
