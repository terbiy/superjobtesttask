'use strict';

/**
 * @ngdoc directive
 * @name superJobTestTaskApp.directive:favourites
 * @description
 * # favourites
 */
angular.module('superJobTestTaskApp')
  .directive('favourites', function () {

    return {
      templateUrl: './views/favourites.html',
      restrict: "A",
      controller: function ($scope, favourites) {
        $scope.inFavourites = favourites.inFavourites;
        $scope.toggleFavourite = favourites.toggleFavourite;
      }
    };
  });
