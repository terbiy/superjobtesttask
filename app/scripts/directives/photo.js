'use strict';

/**
 * @ngdoc directive
 * @name superJobTestTaskApp.directive:photo
 * @description
 * # photo
 */
angular.module('superJobTestTaskApp')
  .directive('photo', function () {
    return {
      template: '<img ng-src="./images/{{candidate.photo}}" alt="">',
      restrict: 'A',
      replace: true
    };
  });
