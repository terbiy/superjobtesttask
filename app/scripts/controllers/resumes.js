'use strict';

/**
 * @ngdoc function
 * @name superJobTestTaskApp.controller:ResumesCtrl
 * @description
 * # ResumesCtrl
 * Controller of the superJobTestTaskApp
 */
angular.module('superJobTestTaskApp')
  .controller('ResumesCtrl', function ($scope, duration, workExperience, getAccessToResume, resumes) {
    resumes.data.query(function (candidates) {
      $scope.candidates = candidates;

      $scope.resumesPresent = candidates.length > 0;

      $scope.stringToDate = duration.stringToDate;
      $scope.workExperience = workExperience;
      $scope.hasAccessToContacts = getAccessToResume.hasAccess;
    });
  })
  .controller('ResumeViewCtrl', function ($scope, $route, $routeParams, duration, workExperience, getAccessToResume, resumes, contacts) {
    resumes.data.query(function (candidates) {
      var resumeID = parseInt($routeParams.id, 10),
          candidate = resumes.getResume(candidates, resumeID);
      $scope.candidate = candidate;

      $scope.getAccessToContacts = getAccessToResume.getAccess;
      $scope.hasAccessToContacts = getAccessToResume.hasAccess;

      $scope.workExperienceString = workExperience.getTotalTimeEmployedString(candidate.workExperience);
      $scope.hasWorkExperience = candidate.workExperience.length > 0;
      $scope.hasEducation = candidate.education.length > 0;
      $scope.getYearsAndMonthsString = workExperience.getYearsAndMonthsString;
      $scope.countDurationAndGetYearsAndMonthsString = workExperience.countDurationAndGetYearsAndMonthsString;
      $scope.getFromToString = workExperience.getFromToString;

      contacts.candidatesInfo.query(function (info) {
        var candidateContacts = contacts.getCandidateInfo(info, resumeID);

        $scope.candidateContacts = candidateContacts;
      });
    });

    $scope.duration = duration;
  });
