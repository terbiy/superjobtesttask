'use strict';

/**
 * @ngdoc function
 * @name superJobTestTaskApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the superJobTestTaskApp
 */
angular.module('superJobTestTaskApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
